<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column stacked for BIP'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_bip.png',
  'theme' => 'twocol_bip',
  'css' => 'twocol_bip.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
