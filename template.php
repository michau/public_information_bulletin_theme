<?php

function bip_theme_preprocess_html(&$variables) {
  // Add variables for path to theme.
  $variables['base_path'] = base_path();
  $variables['path_to_resbartik'] = drupal_get_path('theme', 'bip_theme');
  
  // Add body classes if certain regions have content.
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function bip_theme_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function bip_theme_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
  
  
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function bip_theme_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'bip_theme') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function bip_theme_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function bip_theme_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  // prepare metadata information
    $metadata = array();

    $signed_by = field_get_items('node',$variables['node'],'field_signed');
    $signed_by_val = _fill_empty(field_view_value('node',$variables['node'],'field_signed', $signed_by[0]));
    $signed_by_label = 'Dokument podpisany przez:';
    $metadata['signed_by'] = _wrap_metadata( $signed_by_label, $signed_by_val['#markup'] );

    $signed_date = field_get_items('node',$variables['node'],'field_signed_date');
    $signed_date_val = _fill_empty(field_view_value('node',$variables['node'],'field_signed_date', $signed_date[0]));
    $signed_date_label = 'Data podpisania dokumentu:';
    $metadata['signed_date'] = _wrap_metadata( $signed_date_label, $signed_date_val['#markup'] );

    $published_by = user_load($variables['node']->revision_uid);
    $published_by_val = format_username($published_by);
    $published_by_label = 'Dokument opublikowany przez:';
    $metadata['published_by'] = _wrap_metadata( $published_by_label, $published_by_val );
    
    $published_date_val = format_date($variables['node']->revision_timestamp);
    $published_date_label = 'Data publikacji dokumentu:';
    $metadata['published_date'] = _wrap_metadata( $published_date_label, $published_date_val );    

    $statistics = statistics_get($variables['node']->nid);
    if ($statistics) {
      $statistics_label = 'Liczba odwiedzin strony:';
      $metadata['visited_count'] = _wrap_metadata( $statistics_label, $statistics['totalcount'] );
    }
            

    $variables['metadata'] = $metadata;
  
  
}

function _fill_empty( $value ) {
   return empty( $value ) || empty( $value['#markup'] ) ? array( '#markup' => 'Brak danych' ) : $value;
}

function _wrap_metadata( $label, $value ) {
  $output = '<div class="metadata-field">';
  $output .= '<span class="field-label">' . $label . '</span>';
  $output .= '<span class="field-value">' . $value . '</span>';  
  $output .= '</div>';
  return $output;
}

/**
 * Override or insert variables into the block template.
 */
function bip_theme_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function bip_theme_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function bip_theme_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}

/**
 * Returns HTML for a start/end date combination on form. 
 * Overwrite of behaviour from sites/all/modules/date/date.theme
 */

function bip_theme_date_combo( $variables ) {
  // for some reason, field description is being generated twice,
  // so unsetting it here first
  $variables['element']['#description'] = '';
  $elem = theme('form_element', $variables);
  return $elem;
}

